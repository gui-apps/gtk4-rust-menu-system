use gtk4 as gtk;
use gtk::gio;

fn main() {
    gio::compile_resources(
        "src/resources",
        "src/resources/resources.gresource.xml",
        "resources.gresource",
    );
}
